// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
// Знаковые и беззнаковые переменные
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

#include<iostream>
#include<limits.h>
using namespace std;

int main() {
	int var = 255; // Знаковая переменная
	unsigned int var2 = 200; // Беззнаковая переменная
	cout << -var << "\t" << var2 << endl;
	int varMin = INT_MIN;
	int varMax = INT_MAX;
	cout << varMin << "\t" << varMax << endl;
	unsigned uVarMin = 0;
	unsigned uVarMax = UINT_MAX;
	cout << uVarMin << "\t" << uVarMax << endl;
	return 0;	
}
// Output:
/*
*/
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
