// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
// Булевый тип данных
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//

#include<iostream>
using namespace std;

int main() {
	cout << boolalpha;
	bool a = false;
	bool b = true;
	cout << a << "\t" << b << endl;
	int aa = 5, bb = 12;
	
	bool result = (aa == bb) ? true : false;
	cout << "aa равно bb? " << result << endl;
	
	result = (aa < bb) ? true : false;
	cout << "aa меньше bb? " << result << endl;
	
	result = (aa > bb) ? true : false;
	cout << "aa больше bb? " << result << endl;
	
	result = (aa <= bb) ? true : false;
	cout << "aa меньше и равно bb? " << result << endl;
	
	result = (aa >= bb) ? false : true;
	cout << "aa больше и равно bb? " << result << endl;

	result = (aa != bb) ? true : false;
	cout << "aa не равно bb? " << result << endl;

	result = (aa < bb) || (aa > bb);
	cout << "Сравнение ИЛИ " << result << endl;

	result = (aa < bb) && (aa > bb);
	cout << "Сравнение И " << result << endl;
	return 0;
}
// Output:
/*
*/
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-//
// END FILE
