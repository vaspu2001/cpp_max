#include <iostream>
using namespace std;

int main() {
	int val = 0;
	while (true) {
		cout << "Enter 0 for exit cycle" << endl;
		cin >> val;
		while (val == 0) {
			exit(0);
		}
	}
	cout << "You enter: " << val << endl; 
	
	return 0;
}
