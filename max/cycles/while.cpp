#include <iostream>
using namespace std;

int main() {

	char word = 'a' - 1;
	while (word < 'z') {
		++word;
		cout << word << " ";
	}
	cout << endl;
	
	return 0;
}
